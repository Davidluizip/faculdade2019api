define({ "api": [
  {
    "type": "post",
    "url": "/api/v1/sign-in",
    "title": "Autenticação",
    "version": "1.0.0",
    "name": "Login",
    "group": "Auth",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>Para se autenticar, você precisará ter um usuário no banco de dados.</p>",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email do usuário</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>senha do usuário</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "const data = {\n  \"email\": \"usuario@usuario.com\",\n  \"password\": \"suasenha\"\n};",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>O token que deve ser usado para acessar os outros endpoints</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token_expires",
            "description": "<p>expiração datetime (YYYY-MM-DDTHH:mm:ssZ)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resfreshToken",
            "description": "<p>O resfreshToken que deve ser usado para solicitar um novo token valido</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resfreshToken_expires",
            "description": "<p>resfreshToken_expires datetime (YYYY-MM-DDTHH:mm:ssZ)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user",
            "description": "<p>O id do usuário</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success response:",
          "content": " HTTPS 200 OK\n {\n  \"token\": \"JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9 ... e o resto do token aqui\",\n  \"expires\": \"2017-10-28T14:50:17+00:00\",\n  \"resfreshToken\": \"QiLCJhbGciOiJIUz\",\n  \"resfreshToken_expires\": \"2017-10-28T14:50:17+00:00\",\n  \"user\": \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "dist/router/AuthRouter.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/logout/:id",
    "title": "GET id",
    "name": "LafargeHolcim",
    "group": "Logout",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n          {\n              0\n          }\n\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "dist/router/AuthRouter.js",
    "groupTitle": "Logout"
  },
  {
    "type": "post",
    "url": "/api/v1/notification",
    "title": "NotificationEmail",
    "version": "1.0.0",
    "name": "LafargeHolcim",
    "group": "NotificationEmail",
    "permission": [
      {
        "name": "private"
      }
    ],
    "description": "<p>Para se autenticar, você precisará ter um usuário no banco de dados.</p>",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email do usuário</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "const data = {\n  \"email\": \"usuario@usuario.com\"\n};",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email do usuário</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success response:",
          "content": " HTTPS 200 OK\n {\n  \"email\": \"user@user.com\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "dist/router/NotificationRouter.js",
    "groupTitle": "NotificationEmail"
  },
  {
    "type": "post",
    "url": "/api/v1/auth/refresh-token",
    "title": "Refresh Token",
    "version": "1.0.0",
    "name": "LafargeHolcim",
    "group": "ReFreshToken",
    "permission": [
      {
        "name": "private"
      }
    ],
    "description": "<p>Para gerar um novo token valido, adicionar no header o refreshToken e no body o email do usuário.</p>",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email do usuário</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "const data = {\n  \"email\": \"usuario@usuario.com\"\n};",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>O token que deve ser usado para acessar os outros endpoints</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token_expires",
            "description": "<p>expiração datetime (YYYY-MM-DDTHH:mm:ssZ)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resfreshToken",
            "description": "<p>O resfreshToken que deve ser usado para solicitar um novo token valido</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resfreshToken_expires",
            "description": "<p>resfreshToken_expires datetime (YYYY-MM-DDTHH:mm:ssZ)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success response:",
          "content": " HTTPS 200 OK\n {\n  \"token\": \"JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9 ... e o resto do token aqui\",\n  \"expires\": \"2017-10-28T14:50:17+00:00\",\n  \"resfreshToken\": \"QiLCJhbGciOiJIUz\",\n  \"resfreshToken_expires\": \"2017-10-28T14:50:17+00:00\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "dist/router/AuthRouter.js",
    "groupTitle": "ReFreshToken"
  },
  {
    "type": "post",
    "url": "/api/v1/user/reset-password",
    "title": "",
    "version": "1.0.0",
    "name": "LarfageHolcim",
    "group": "ResetPassword",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>para solicitar o reset de senha, adicionar no body somente o CPF do usuário.</p>",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email do usuário</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "const data = {\n  \"CPF\": 12345678901\n};",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>Code 8 de mensagem enviada com sucesso!</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success response:",
          "content": "    HTTPS 200 OK\n{\n   \"code\": 8,\n   \"message\": \"Message sent successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "dist/router/UserRouter.js",
    "groupTitle": "ResetPassword"
  }
] });
