"use strict";
import { Router, Request, Response, NextFunction } from "express";
import { Operation } from '../service/ControllerApi/Operation';
import { validateParameter } from "../service/ControllerApi/validateParameter";
import { EmailService } from "./../service/ControllerEmail/EmailService";
import { DomainClass } from "../service/ControllerApi/DomainClass";

class NotificationRouter {

    router: Router;
    domain: DomainClass;
    status: number;
    constructor() {
        this.domain = DomainClass.NotificationRouter;
        this.router = Router();
        this.routes();
    }
    private _notificationEmail = async (req: Request, res: Response, next: NextFunction) => {
        try {
            await new validateParameter({ operation: Operation.Email, body: req.body }).checkBody();

           // await new EmailService({ email: req.body.email }).sendMail();

            return res.status(200).json({ data: "n" });
        } catch (_err) {
            return await next(_err);
        }
    }
    routes() {

        /**
      * @api {post} /api/v1/notification NotificationEmail
      * @apiVersion 1.0.0
      * @apiName PadProjSotf
      * @apiGroup NotificationEmail
      * @apiPermission private
      * @apiDescription Para se autenticar, você precisará ter um usuário no banco de dados.
      *
      * @apiParam (Request body) {String} email email do usuário
      *
      * @apiExample {js} Example usage:
      * const data = {
      *   "email": "usuario@usuario.com"
      * };
      *
      * @apiSuccess {String} email email do usuário
      *
      * @apiSuccessExample {json} Success response:
       *     HTTPS 200 OK
       *     {
       *      "email": "user@user.com"
       *    }
      */
        this.router.post('/', this._notificationEmail);

    }
}
export default new NotificationRouter().router