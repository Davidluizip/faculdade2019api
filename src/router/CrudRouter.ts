import { Request, Response, NextFunction } from "express";

export default class CrudRouter {

    private service: any;
   
    constructor(service) {
        this.service = service;
    }

    public async getAll(req: Request, res: Response, next: NextFunction){
        try {
            const result = await this.service.getAll({ pagination: req['pagination'], orderby: req['order'] });

            return res.status(200).json(result);
        } catch (_err) {
            return next(_err);
        }
    }
    
    public async getById(req: Request, res: Response, next: NextFunction){
        try {
            const result = await this.service.getById(req.params.id);

            return res.status(200).json(result);
        } catch (_err) {
            return next(_err);
        }
    }

    public async put(req: Request, res: Response, next: NextFunction){
        try {
            const result = await this.service.update(req.params.id, req.body);

            return res.status(200).json(result);
        } catch (_err) {
            return next(_err);
        }
    }

    public async post(req: Request, res: Response, next: NextFunction){
        try {
            const result = await this.service.create(req.body);

            return res.status(200).json(result);
        } catch (_err) {
            return next(_err);
        }
    }

    public async delete(req: Request, res: Response, next: NextFunction){
        try {
            const result = await this.service.delete(req.params.id);

            return res.status(200).json(result);
        } catch (_err) {
            return next(_err);
        }
    }
}