"use strict";
import { Router, Request, Response, NextFunction } from "express";
import { AuthService } from '../service/AuthService';
import { Operation } from '../service/ControllerApi/Operation';
import { validateParameter } from "../service/ControllerApi/validateParameter";
import { DomainClass } from "../service/ControllerApi/DomainClass";

class AuthRouter {

    router: Router;
    domain: DomainClass;
    status: number;
    
    constructor() {
        this.domain = DomainClass.AuthRouter
        this.router = Router();
        this.routes();
    }

    private login = async (req: Request, res: Response, next: NextFunction) => {
        try {
            await new validateParameter({ operation: Operation.login, body: req.body }).checkBody();

            const resultLogin = await AuthService.instance(req.body).validarCredenciais();

            return res.status(200).json(resultLogin);

        } catch (_err) {
            return next(_err);
        }
    }
    private logout = async (req: Request, res: Response, next: NextFunction) => {
        try {

            const resultLogout = await AuthService.instance(req.body).logout();

            return res.status(200).json(resultLogout);

        } catch (_err) {
            return next(_err);
        }
    }
    private refreshToken = async (req: Request, res: Response, next: NextFunction) => {
        try {
            await new validateParameter({ operation: Operation.RefreshToken, body: req.body }).checkBody();

            const resultRefresh = await AuthService.instance(req.body).genereteRefreshToken(req.user.id);

            return res.status(200).json({ data: resultRefresh });
        } catch (_err) {
            return next(_err);
        }
    }
    routes() {
        /**
          * @api {get} /logout/:id GET id
          * @apiName PadProjSotf
          * @apiGroup Logout
          *
          * @apiParam {Number} id unique ID.
     
          * 
          * @apiSuccessExample {json} Success-Response:
          * HTTP/1.1 200 OK
          * {
          *           {
          *               0      
          *           }
          *       
          *   }
          */
        this.router.get('/logout/:id', this.logout);

        /**
      * @api {post} /api/v1/sign-in Autenticação
      * @apiVersion 1.0.0
      * @apiName Login
      * @apiGroup Auth
      * @apiPermission public
      * @apiDescription Para se autenticar, você precisará ter um usuário no banco de dados.
      *
      * @apiParam (Request body) {String} email email do usuário
      * @apiParam (Request body) {String} password senha do usuário
      *
      * @apiExample {js} Example usage:
      * const data = {
      *   "email": "usuario@usuario.com",
      *   "password": "suasenha"
      * };
      *
      * @apiSuccess {String} token O token que deve ser usado para acessar os outros endpoints
      * @apiSuccess {String} token_expires expiração datetime (YYYY-MM-DDTHH:mm:ssZ)
      * @apiSuccess {String} resfreshToken O resfreshToken que deve ser usado para solicitar um novo token valido
      * @apiSuccess {String} resfreshToken_expires resfreshToken_expires datetime (YYYY-MM-DDTHH:mm:ssZ)
      * @apiSuccess {String} user O id do usuário
      *
      * @apiSuccessExample {json} Success response:
       *     HTTPS 200 OK
       *     {
       *      "token": "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9 ... e o resto do token aqui",
       *      "expires": "2017-10-28T14:50:17+00:00",
       *      "resfreshToken": "QiLCJhbGciOiJIUz",
       *      "resfreshToken_expires": "2017-10-28T14:50:17+00:00",
       *      "user": "1"
       *    }
      */
        this.router.post('/', this.login);
        /**
      * @api {post} /api/v1/auth/refresh-token Refresh Token
      * @apiVersion 1.0.0
      * @apiName PadProjSotf
      * @apiGroup ReFreshToken
      * @apiPermission private
      * @apiDescription Para gerar um novo token valido, adicionar no header o refreshToken e no body o email do usuário.
      *
      * @apiParam (Request body) {String} email email do usuário
      *
      * @apiExample {js} Example usage:
      * const data = {
      *   "email": "usuario@usuario.com"
      * };
      *
      * @apiSuccess {String} token O token que deve ser usado para acessar os outros endpoints
      * @apiSuccess {String} token_expires expiração datetime (YYYY-MM-DDTHH:mm:ssZ)
      * @apiSuccess {String} resfreshToken O resfreshToken que deve ser usado para solicitar um novo token valido
      * @apiSuccess {String} resfreshToken_expires resfreshToken_expires datetime (YYYY-MM-DDTHH:mm:ssZ)
      *
      * @apiSuccessExample {json} Success response:
       *     HTTPS 200 OK
       *     {
       *      "token": "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9 ... e o resto do token aqui",
       *      "expires": "2017-10-28T14:50:17+00:00",
       *      "resfreshToken": "QiLCJhbGciOiJIUz",
       *      "resfreshToken_expires": "2017-10-28T14:50:17+00:00",
       *    }
      */
        this.router.post('/refresh-token', this.refreshToken);
    }
}
export default new AuthRouter().router