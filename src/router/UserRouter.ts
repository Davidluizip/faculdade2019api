"use strict";
import { Router, Request, Response, NextFunction } from "express";
import { Operation } from '../service/ControllerApi/Operation';
import { validateParameter } from "../service/ControllerApi/validateParameter";
import { DomainClass } from "../service/ControllerApi/DomainClass";
import { UserService } from "../service/UserService";
import { Message } from "../service/ControllerApi/Message";
import CrudRouter from "./CrudRouter";
import CrudService from "../service/CrudService";
import { User } from "../models/User";

class UserRouter extends CrudRouter{

    router: Router;
    domain: DomainClass;
    
    constructor() {
        super(new CrudService(User));
        this.domain = DomainClass.UserRouter
        this.router = Router();
        this.routes();
    }

    public async put(req: Request, res: Response, next: NextFunction){
        delete req.body.password;
        delete req.body.ismaster;
        super.put(req, res, next);
    } 

    private resetPassword = async (req: Request, res: Response, next: NextFunction) => {
        try {
            await new validateParameter({ operation: Operation.RESETPASSWORD, body: req.body }).checkBody();
            
            const user = UserService.instance(req.body);

            await user.validateUser();
            
            await user.generateNewPassword().save();

            await user.notifyUserNewPassword();

            return res.status(200).json(Message.MESSAGESUCCESSEMAIL);

        } catch (_err) {
            return next(_err);
        }
    }

    routes() {

        /**
      * @api {post} /api/v1/user/reset-password
      * @apiVersion 1.0.0
      * @apiName PadProjSotf
      * @apiGroup ResetPassword
      * @apiPermission public
      * @apiDescription para solicitar o reset de senha, adicionar no body somente o CPF do usuário. 
      *
      * @apiParam (Request body) {String} email email do usuário
      *
      * @apiExample {js} Example usage:
      * const data = {
      *   "CPF": 12345678901
      * };
      *
      * @apiSuccess {Number} code Code 8 de mensagem enviada com sucesso!
      * @apiSuccess {String} message Mensagem
      *
      * @apiSuccessExample {json} Success response:
      *     HTTPS 200 OK
      *{
      *    "code": 8,
      *    "message": "Message sent successfully"
      *}
      */
        this.router.post('/reset-password', this.resetPassword);

        this.router.get('/', this.getAll.bind(this));
        this.router.get('/:id', this.getById.bind(this));
        this.router.post('/', this.post.bind(this));
        this.router.put('/:id', this.put.bind(this));
        this.router.delete('/:id', this.delete.bind(this));
        
    }
}
export default new UserRouter().router