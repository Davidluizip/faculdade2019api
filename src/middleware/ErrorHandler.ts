import { Request, Response, NextFunction } from "express";
import { ApiException } from "../service/ControllerApi/ApiException";

export class ErrorHandler {
    constructor() { }
    intercept = async (err: any, req: Request, res: Response, next: NextFunction) =>{

        const error = await ApiException.instance({ error: err }).inspectError();

        let status = error.status;

        delete error.status;

        res.status(status).json(error);
    }
}