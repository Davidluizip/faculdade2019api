import * as passport from "passport";
import { Strategy, ExtractJwt } from "passport-jwt";
import { ConnectionData } from "../infrastructure/ConnectionData";
import { User } from "../models/User";

class Auth {

    public initialize = () => {
        passport.use("jwt", this.getStrategy());
        return passport.initialize();
    }

    public authenticate = (callback) => passport.authenticate("jwt", { session: false, failWithError: true }, callback);

    private getStrategy = (): Strategy => {
        const params = {
            secretOrKey: ConnectionData.secretToken,
            jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
            passReqToCallback: true
        };

        return new Strategy(params, (req, payload: any, done) => {
            User.scope('sign').findOne({where: {"email": payload.email} })
            .then(user=>{

                if (user === null) {
                    return done(null, false, { message: "The user in the token was not found" });
                }
                if ((user.token_user.token !== req.headers.authorization) && (user.token_user.refresh_token !== req.headers.authorization)) {
                    return done(null, false, { message: "Invalid token" });
                }
                return done(null, { id: user.id, email: user.email });
            })
            .catch(err=>{
                console.error(err);
                return done(err);
            })
        });
    }

}
export default new Auth();