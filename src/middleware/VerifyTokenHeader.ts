import { Request, Response, NextFunction } from 'express';
import { Message } from '../service/ControllerApi/Message';
import { MessagingService } from '../service/ControllerApi/MessagingService';
import { ErroType } from '../service/ControllerApi/ErroType';
import { DomainClass } from '../service/ControllerApi/DomainClass';

const auth = require("./Auth").default;
const exception: MessagingService = new MessagingService({
    erroType: ErroType.manipulated,
    domain: DomainClass.VerifyTokenHeader
});

export class VerifyTokenHeader {
    constructor() { }
    async verifyTokenHeader(req: Request, res: Response, next: NextFunction) {
        try {
            if (req.path.includes('/api/v1/sign-in') || req.path.includes('/api/doc') || req.path.includes('/api/v1/user/reset-password')) {
                return next();
            }

            return auth.authenticate((err, user, info) => {

                if (err) { return next(err); }

                if (!user) {
                    if (info.name === "TokenExpiredError") {
                        throw exception.message(Message.TokenExpires);
                    } else {
                        return res.status(401).json({ message: info.message });
                    }
                }
                req['user'] = user;

                return next();
            })(req, res, next);

        } catch (_err) {
            return next(_err);
        }

    }
}