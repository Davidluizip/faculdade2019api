import {Request, Response, NextFunction } from "express";

export class RemoveId{
    constructor(){}
    removeID(req: Request,res: Response,next: NextFunction){
        delete req.body.id;
        next();
    }
}