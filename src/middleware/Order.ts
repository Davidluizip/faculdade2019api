import {Router, Request, Response, NextFunction} from 'express';

export class Order {

    constructor() {}

    order(req: Request, res: Response, next: NextFunction) {

        let order_field = req.query.order_field;
        if (order_field) {
            let order = {};

            order["field"] = order_field;

            let order_type = req.query.order_type;
            if (!order_type) {
                order_type = 'asc';
            }
            order["type"] = order_type;

            req['order'] = order;
        } else {
            let order = {};
            order["field"] = 'id';
            order["type"] = 'asc'
            req['order'] = order;
            //req['order'] = null;
        }
 
        return next();
    }
}
