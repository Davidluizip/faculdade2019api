import {Router, Request, Response, NextFunction} from 'express';

export class Filter {

    constructor() {}

    filter(req: Request, res: Response, next: NextFunction) {
        let reservedWord : Array<String> = ['page','per_page','order_field','order_type'];

        let filter = {};
        let qtd = 0;

        for(let key in req.query) {
            let result = reservedWord.filter(item => item == key);
            if (result.length == 0 && req.query[key] != "") {
                filter[key] = req.query[key];
                qtd++;
            }
        }
        if (qtd > 0) {
            req['filtro'] = filter;
        } else {
            req['filtro'] = null;
        }
        return next();
    }
}
