import {Router, Request, Response, NextFunction} from 'express';

export class Pagination {

    constructor() {}

    pagination(req: Request, res: Response, next: NextFunction) {
        
        //let page = req.query.page;
        let page = Number(req.headers['page']);
        if (page) {
            let pagination = {};

            if (page < 1) {
                page = 1;
            }
            pagination["page"] = +page;

            let per_page = Number(req.headers['per_page']);//req.query.per_page;
            if (!per_page) {
                per_page = 10;
            }
            pagination["per_page"] = + per_page;

            pagination["limit"] = + per_page;
            pagination["offset"] = (page - 1) * per_page;
        
            req['pagination'] = pagination;
        } else {
            let pagination = {};

            pagination["page"] = + 1;

            pagination["per_page"] = + 10;

            pagination["limit"] = 10;
            pagination["offset"] = 0;

            req['pagination'] = pagination;
        }
        return next();
    }
}
