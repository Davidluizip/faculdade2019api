import { User } from "../models/User";
import * as fs from "fs";
import * as path from "path";
import { Token } from "../models/Token";

export class Scripts {
    constructor(){
    }
    static newIntance(){
        return new Scripts();
    }
    public async execute(){
        await this.user();
    }
    private async user(){
//        console.log("path", __dirname.replace("dist\\util",'sgbd\\user.json'));
//        const file = fs.readFileSync(__dirname.replace("dist\\util",'sgbd\\user.json'), 'utf8')

     const _user: User = await User.create<User>({
            "name": "admin",
            "password": "teste",
            "CPF": 12345678901,
            "email": "t@t.com",
            "active": "Y",
            "term": true,
            "ismaster": true
        })

        await Token.create({
            user_id: _user.id
        })
        console.log("User Created success!")
    }
}