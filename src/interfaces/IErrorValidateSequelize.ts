interface IErrorValidateSequelize{
    message: string
    type: string
    path: string
    value: string
    origin: string
}