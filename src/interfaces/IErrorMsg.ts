import { IMessage } from "./IMessage";

export class IErrorMsg{
    level: number;
    code: number;
    status: number;
    message: IMessage | string;
}

