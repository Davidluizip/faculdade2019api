interface IErrorSequelize{
    name: string
    parent: {
        code: string,
        errno: number,
        sqlState: string,
        sqlMessage: string,
        sql: string
    }
    original: {
        code: string,
        errno: number,
        sqlState: string,
        sqlMessage: string,
        sql: string
    }
    sql: string
}