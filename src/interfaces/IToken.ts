export class IToken{
    token: string;
    token_expires: string;
    refresh_token: string;
    refresh_token_expires: string;
}