interface ICredentials {
    id: number;
    email: string;
    password: string;
}