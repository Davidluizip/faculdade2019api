"use strict"

import { Scripts } from './util/Scripts';
import * as bodyParser from "body-parser";
import * as express from "express";
import * as logger from "morgan";
import * as helmet from "helmet";
import * as cors from "cors";

import connection from './infrastructure/ConnectBD';

import { Pagination } from "./middleware/Pagination";
import { Order } from "./middleware/Order";
import { Filter } from "./middleware/Filter";
import { RemoveId } from './middleware/RemoveId';
import { VerifyTokenHeader } from "./middleware/VerifyTokenHeader";
import { ErrorHandler } from "./middleware/ErrorHandler";

import AuthRouter from "./router/AuthRouter";
import NotificationRouter from "./router/NotificationRouter";
import UserRouter from "./router/UserRouter";

const result = require("dotenv").config();
if (result.error) {
    throw result.error;
}
const auth = require("./middleware/Auth").default;

class App {

    public express: express.Application;

    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
    }

    private middleware(): void {
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(auth.initialize());
        this.express.use(logger("dev"));
        //this.express.use(fileUpload());
        //this.express.use(methodOverride());
        this.express.use(helmet());
        this.express.use(cors({
            origin: "*",//["http://localhost:4200"],
            methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
            allowedHeaders: ["Content-Type", "Authorization", 'page', 'per_page', 'limit']
        }));
        this.express.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });

    }
    private async routes() {
        console.log('Configurando o SGBD...');
        const conn = await connection();
        const baseURI: string = process.env.BASEURL;

        console.log('Sincronizando SGBD');

        // conn.query('SET FOREIGN_KEY_CHECKS = 0', {raw: true}).then(function(results) {
        //     conn.sync({force: true});
        // });

        conn
            .sync({ force: true })
            .then(async () => {

                Scripts.newIntance().execute();

                console.log('Configurando as rotas da API');

                this.express.use(new Pagination().pagination);
                this.express.use(new Order().order);
                this.express.use(new Filter().filter);
                this.express.use(new RemoveId().removeID);
                // this.express.use(new VerifyTokenHeader().verifyTokenHeader);

                //Rotas  Public || static
                this.express.use('/api/doc', express.static(__dirname.replace('dist', 'doc')));
                this.express.use(`${baseURI}/sign-in`, AuthRouter);

                //this.express.use(`${baseURI}/notification`, ResetPasswordRouter);

                //Rotas Privadas
                this.express.use(`${baseURI}/notification`, NotificationRouter);
                this.express.use(`${baseURI}/auth`, AuthRouter);
                this.express.use(`${baseURI}/user`, UserRouter);

                // backend ...
                let routerBackEnd = express.Router();
                routerBackEnd.all('/*', (req, res, next) => {
                    console.log(req.url);
                    console.log("Route not Found");
                    res.status(501).json({ info: "Route not Found" })
                });

                this.express.use('/*', routerBackEnd);

                console.log('Rotas Configuradas.. tudo pronto para API!');

                this.express.use(new ErrorHandler().intercept);

                // console.log('Executando scripts');
                // new Scripts().execute();
            })
            .catch((err) => {
                console.log('Erro ao tentar conectar com o banco de dados!');
                console.error(err);
            })
    }
}
export default new App().express