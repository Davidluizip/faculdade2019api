"use strict"
import { Table, Model, Column, DataType, Scopes } from "sequelize-typescript";

@Scopes({
    full: {
        attributes: ['id', 'name']
    }
})
@Table({
    timestamps: true,
    tableName: "logs"
})
export class Logs extends Model<Logs>{

    @Column({
        type: DataType.INTEGER,
        primaryKey: true,
        autoIncrement: true
    })
    id: number;
 
    @Column({
        type: DataType.INTEGER,
        allowNull: false
    })
    LevelErr: number;
    
    @Column({
        type: DataType.INTEGER,
        allowNull: false
    })
    domain: number;

    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    message: string;

    engine: 'InnoDB';
    charset: 'latin1';
}