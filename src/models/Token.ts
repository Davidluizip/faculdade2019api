"use strict"
import { Table, Model, Column, DataType, ForeignKey, Scopes } from "sequelize-typescript";
import { User } from "./User";

@Scopes({
    full: {
        attributes: ['id', 'token', 'token_expires', 'refresh_token', 'refresh_token_expires']
    }
})
@Table({
    timestamps: true,
    tableName: "token"
})
export class Token extends Model<Token>{

    @Column({
        type: DataType.INTEGER,
        primaryKey: true,
        autoIncrement: true
    })
    id: number;

    @ForeignKey(() => User)
    @Column({
        type: DataType.INTEGER,
        allowNull: false
    })
    user_id: number;

    @Column({
        type: DataType.STRING,
        allowNull: true
    })
    token: string;

    @Column({
        type: DataType.DATE,
        allowNull: true
    })
    token_expires: Date;

    @Column({
        type: DataType.STRING,
        allowNull: true
    })
    refresh_token: string;
    
    @Column({
        type: DataType.DATE,
        allowNull: true
    })
    refresh_token_expires: Date;
    
    engine: 'InnoDB';
    charset: 'latin1';
}