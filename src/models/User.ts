"use strict"
import { BeforeCreate, Table, Model, Column, DataType, HasOne, Scopes } from "sequelize-typescript";

import * as bcrypt from "bcrypt";
import { Token } from "./Token";

@Scopes({
    full: {
        attributes: ['id', 'name', 'email', 'term', 'ismaster', 'CPF']
    },
    sign: {
        attributes: ['id', 'name', 'email', 'password', 'term', 'ismaster'],
        include: [
            {
                model: () => Token,
                required: true
            }
        ]
    }
})
@Table({
    timestamps: true,
    tableName: "user"
})
export class User extends Model<User>{

    @Column({
        type: DataType.INTEGER,
        primaryKey: true,
        autoIncrement: true
    })
    id: number;

    @Column({
        type: DataType.STRING(100),
        allowNull: false
    })
    name: string;

    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    password: string;

    @Column({
        type: DataType.STRING(50),
        allowNull: false,
        unique: true
    })
    CPF: number;

    @Column({
        type: DataType.STRING(150),
        allowNull: false,
        unique: true
    })
    email: string;

    @Column({
        type: DataType.BOOLEAN,
        defaultValue: false
    })
    online: boolean;

    @Column({
        type: DataType.BOOLEAN,
        defaultValue: false
    })
    type: boolean;

    @Column({
        type: DataType.STRING(1),
        defaultValue: false
    })
    active: string;

    @Column({
        type: DataType.BOOLEAN,
        defaultValue: false
    })
    term: boolean;

    @Column({
        type: DataType.STRING
    })
    token: string;

    @Column({
        type: DataType.DATE
    })
    token_expire: Date;

    @Column({
        type: DataType.BOOLEAN
    })
    ismaster: boolean;

    @BeforeCreate
    static cryptPasswordCreate(user: User) {
        if (user.password != null) {
            const salt = bcrypt.genSaltSync(10);
            user.password = bcrypt.hashSync(user.password, salt);
        }
    }

    async comparePassword(candidatePassword: string): Promise<Boolean> {
        let password = this.password;
        return await bcrypt.compareSync(candidatePassword, password);
    };

    @HasOne(() => Token)
    token_user: Token;

    engine: 'InnoDB';
    charset: 'latin1';
}