import { ConnectionData } from "./ConnectionData";
import { Sequelize } from "sequelize-typescript";

class ConnectBD {

    _base: any;

    public async Conn() {

        try {
            console.log("Conectando ao HOST :: ", ConnectionData.host);
            console.log("configurando ao ambiente :: ", process.env.NODE_ENV);
            const sequelize = await new Sequelize({
                name: ConnectionData.database,
                dialect: ConnectionData.dialect,
                /* dialectOptions: {
                    ssl: {
                        require: true
                    }
                }, */
                host: ConnectionData.host,
                port: ConnectionData.port,
                username: ConnectionData.username,
                password: ConnectionData.password,
                operatorsAliases: false,
                logging: true,
                modelPaths: [
                    __dirname + './../models'
                ]
            });

            return sequelize;

        } catch (error) {
            console.error(error);
            return error;
        }
    }
}
export default new ConnectBD().Conn;