export class ConnectionData {

    private static _dialect: string = 'mysql';
    private static _secretToken: string = "32adbe77c73045a7bdfb4f8c6625053f";
    private static _secretRefreshToken: string = "3iadbe77c75665a7bdfb6f7c6625053f";

    //Configurações de SGBD de DESENVOLVIMENTO LOCAL
    private static _database_dev: string = 'PadProjSotf';
    private static _host_dev: string = 'localhost';
    private static _port_dev: number = 3306;
    private static _username_dev: string = 'root';
    private static _password_dev: string = '';
    private static _apidoc: string = __dirname + "/public";
    
    //Configurações de SGBD de DESENVOLVIMENTO AWS
    private static _database_aws: string = '';
    private static _host_aws: string = '';
    private static _port_aws: number = 3306;
    private static _username_aws: string = '';
    private static _password_aws: string = '';
    private static _apidoc_aws: string = __dirname + "/public";

    //Configurações de SGBD de HOMOLOGAÇÃO

    private static _database_hmg: string = '';
    private static _host_hmg: string = '';
    private static _port_hmg: number = 0;
    private static _username_hmg: string = '';
    private static _password_hmg: string = '';
    private static _apidoc_hmg: string = __dirname + "/public";

    //Configurações de SGBD de PRODUÇÃO

    private static _database_prod: string = '';
    private static _host_prod: string = '';
    private static _port_prod: number = 0;
    private static _username_prod: string = '';
    private static _password_prod: string = '';
    private static _apidoc_prod: string = __dirname + "/public";

    private _params: Object = {
        dialect: "mysql",
        logging: (sql) => {
            //logger.info(`[${new Date()}] ${sql}`);
        },
        define: {
            underscored: true
        }
    }
    private _jwtSecret: String = 'AP1';
    private _jwtSession: Object = {
        session: false
    }
    static get dialect() {
        return this._dialect;
    }
    static get secretToken() {
        return this._secretToken;
    }
    static get secretRefreshToken() {
        return this._secretRefreshToken;
    }
    static get database() {
        if (process.env.NODE_ENV === 'production') 
            return this._database_prod;
        else if ((process.env.NODE_ENV === 'hmg') ) 
            return this._database_hmg;
        else if ((process.env.NODE_ENV === 'awsdev') ) 
            return this._database_aws;
        else
            return this._database_dev;
    }
    static get host() {
        if (process.env.NODE_ENV === 'production') 
            return this._host_prod;
        else if ((process.env.NODE_ENV === 'hmg') ) 
            return this._host_hmg;
        else if ((process.env.NODE_ENV === 'awsdev') ) 
            return this._host_aws;
        else 
        return this._host_dev;
    }
    static get username() {
        if (process.env.NODE_ENV === 'production')
            return this._username_prod;
        else if ((process.env.NODE_ENV === 'hmg') ) 
            return this._username_hmg;
        else if ((process.env.NODE_ENV === 'awsdev') ) 
            return this._username_aws;
        else 
        return this._username_dev;
    }
    static get password() {
        ;
        if (process.env.NODE_ENV === 'production')
            return this._password_prod;
        else if ((process.env.NODE_ENV === 'hmg') ) 
            return this._password_hmg;
        else if ((process.env.NODE_ENV === 'awsdev') ) 
            return this._password_aws;
        else 
        return this._password_dev;
    }
    static get port() {
        if (process.env.NODE_ENV === 'production') 
            return this._port_prod;
        else if ((process.env.NODE_ENV === 'hmg') ) 
            return this._port_hmg;
        else if ((process.env.NODE_ENV === 'awsdev') ) 
            return this._port_aws;
        else 
            return this._port_dev;
    }
    static get apidoc() {
        if (process.env.NODE_ENV === 'production')
            return this._apidoc_prod;
        else 
            return this._apidoc;
    }

}