import * as hbs from "handlebars";
import * as nodemailer from 'nodemailer';
import { Logger } from "./../ControllerApi/Logger";
import { ErroType } from "./../ControllerApi/ErroType";
import { DomainClass } from "./../ControllerApi/DomainClass";
import { Templates } from "./Templates/templates";
import { ControllerTemplate } from "./Templates/ControllerTemplates";

export class EmailService {
    private recipient: string;
    private parameters: Object;
    private template: Templates;
    private subject : string =  "assunto padrão";

    constructor(fields: {
        recipient: string,
        parameters?: Object
        template: Templates
    }) {
        Object.assign(this, fields);
        if (!this.parameters) this.parameters = {email: this.recipient};
    }
    set setAssunto(value: string){
        this.subject = value;
    }
    public sendMail = async () => {

        const _template = await ControllerTemplate.instance(this.parameters, this.template).getTemplate();

        const transport = await this.createService();

        transport.sendMail({
            from: process.env.USEREMAIL,
            to: this.recipient,
            subject: this.subject,
            html: _template
        }, async (err, response) => {

            if (err) 
                new Logger({
                LevelErr: ErroType.service,
                domain: DomainClass.EmailService,
                message: `Error sending email to ${this.recipient}`
            }).register();

                transport.close();
                return response;
            //this.sendMail(++i);
        });

    }
    private createService = async () => {
        const transportOptions = {
            service: "Gmail",
            auth: {
                user: process.env.USEREMAIL,
                pass: process.env.PASSEMAIL
            }
        };
        return await nodemailer.createTransport(transportOptions)
        /*         return nodemailer.createTransport({
                    host: 'mail.www15.locaweb.com.br',
                    port: '587',
                    secure: true,
                    auth: {
                        user: 'usuario@provedor.com',
                        pass: 'shhh!!'
                    }
                }); */
    }

}