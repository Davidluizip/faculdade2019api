import * as hbs from "handlebars";
import { Templates } from "./templates";
import { ResetPasswordtemplate } from "./ResetPassword";

export class ControllerTemplate {
    private parameters: Array<string>;
    private template: Templates;

    constructor(fields: {
        parameters: Array<string>;
        template: Templates
    }) {
        Object.assign(this, fields)
    }
    static instance(_parameters, _template) {
        return new ControllerTemplate({ parameters: _parameters, template: _template })
    }
    getTemplate = async () => {
        switch (this.template) {
            case Templates.RESETPASSWORD:
                return await this.addAttr(ResetPasswordtemplate);
                break;

            default:
                break;
        }
    }
    private addAttr = async (HTML) => {
        const templateHBS = hbs.compile(HTML);
        return await templateHBS(this.parameters);
    }
}