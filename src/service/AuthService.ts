"use strict"

import { User } from '../models/User';
import { TokenService } from './TokenService';
import { Operation } from './ControllerApi/Operation';
import { Message } from './ControllerApi/Message';
import { IToken } from '../interfaces/IToken';
import { Token } from '../models/Token';
import { MessagingService } from './ControllerApi/MessagingService';
import { ErroType } from './ControllerApi/ErroType';
import { DomainClass } from './ControllerApi/DomainClass';

const exception: MessagingService = new MessagingService({
    erroType: ErroType.manipulated,
    domain: DomainClass.AuthService
});

export class AuthService {
    private _user: User;
    private _credentials: ICredentials;
    private _token: IToken;

    constructor(credentials: ICredentials) {
        this._credentials = credentials;
    }
    static instance(credentials: ICredentials) {
        return new AuthService(credentials);
    }
    logout = async() =>{
        return await User.update({ active: false},{where: {id: this._credentials.id}});
    }
    async validarCredenciais() {
        try {
            this._user = await User.scope('sign').findOne({ where: { email: this._credentials.email } });

            let TPacesso = await this.verificarDados_user();

            if (TPacesso == Operation.FirstAccess)

                return this.retornaDadosPrimeiroAcesso();

            if (TPacesso == Operation.GenerateToken) {

                await this.compararSenha();

                this._token = TokenService.gerarTokenLogin(this._credentials.email);

                this.update_userLogadoToken();

                return this.retornaDadosLogin();
            }

        } catch (error) {
            throw error;
        }

    }
    async genereteRefreshToken(_id: number) {
        this._user = new User();
        this._user.id = _id;
        this._user.online = true;

        this._token = TokenService.gerarTokenLogin(this._credentials.email);

        await this.update_userLogadoToken();

        return this.retornaDadosLogin();
    }
    private verificarDados_user(): Operation {

        if (!this._user) throw exception.message(Message.UserNotFound);

        if (this._user.online) throw exception.message(Message.UserOnline);

        if (!this._user.term) return Operation.FirstAccess; // Message.user.TermNotAccept;

        return Operation.GenerateToken;
    }
    private async compararSenha() {

        let success = await this._user.comparePassword(this._credentials.password);

        if (!success) throw exception.message(Message.PasswordInvalid);

    }
    private retornaDadosPrimeiroAcesso() {
        return {
            id: this._user.id,
            //email: this._user.login
        }
    }
    private async update_userLogadoToken() {

        try {

            await Token.update<Token>({
                token: this._token.token,
                token_expires: this._token.token_expires,
                refresh_token: this._token.refresh_token,
                refresh_token_expires: this._token.refresh_token_expires
            }, { where: { id: this._user.id } });

            await User.update<User>({
                online: true
            }, { where: { id: this._user.id } });

        } catch (error) {
            console.error(error);
            throw error;
        }
    }
    private retornaDadosLogin() {
        return {
            id: this._user.id,
            email: this._user.email,
            online: this._user.online,
            token: this._token.token,
            token_expires: this._token.token_expires,
            refresh_token: this._token.refresh_token,
            refresh_token_expires: this._token.refresh_token_expires,
        }
    }
}