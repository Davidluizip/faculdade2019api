"use strict"

import { User } from '../models/User';
import { Message } from './ControllerApi/Message';
import { IToken } from '../interfaces/IToken';
import { MessagingService } from './ControllerApi/MessagingService';
import { ErroType } from './ControllerApi/ErroType';
import { DomainClass } from './ControllerApi/DomainClass';
import { EmailService } from './ControllerEmail/EmailService';
import { Templates } from './ControllerEmail/Templates/templates';

const exception: MessagingService = new MessagingService({
    erroType: ErroType.manipulated,
    domain: DomainClass.UserService
});

export class UserService {
    private _user: User;
    private _token: IToken;

    constructor(user: User) {
        this._user = user;
    }
    static instance(user: User) {
        return new UserService(user);
    }
    notifyUser = async (): Promise<void> => {

    }
    notifyUserNewPassword = async () => {
        let user: User = await this.validateUser();

        if (!user) throw exception.message(Message.UserNotFound);

        await new EmailService({
            recipient: user.email,
            template: Templates.RESETPASSWORD
        }).sendMail();

    }
    validateUser = async (): Promise<User> => {
        return await User.findOne<User>({
            where: {
                CPF: this._user.CPF
            }
        }).then(data =>{
            if (!data) throw exception.message(Message.UserNotFound);
            return data;
        })
    }
    _update = async () => {
        let data: User = Object.assign(this._user);
        delete data.id;
        return await User.update(data, { where: { id: this._user } });
    }

    generateNewPassword = (): User => {
        this._user = new User();
        this._user.password = Math.random().toString(36).substring(0, 7);
        return this._user;
    }

}