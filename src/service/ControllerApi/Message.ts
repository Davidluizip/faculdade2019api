export const Message = {
    UserNotFound: {
        code: 0,
        message: "User Not Found"
    },
    UserOnline: {
        code: 1,
        message: "User Not Found"
    },
    TermNotAccept: {
        code: 2,
        message: "User Not Found"
    },
    PasswordInvalid: {
        code: 3,
        message: "User Not Found"
    },
    TokenExpires: {
        code: 4,
        message: "Your token has expired. Please generate a new one"
    },
    TokenGeneric: {
        code: 5,
        message: "User Not Found"
    },
    emailService: {
        code: 6,
        message: "User Not Found"
    },
    parameter: {
        code: 7,
        message: ""
    },
    MESSAGESUCCESSEMAIL: {
        code: 8,
        message: "Message sent successfully"
    }
}