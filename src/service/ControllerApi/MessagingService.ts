"use strict"
import { ErroType } from './ErroType';
import { IErrorMsg } from '../../interfaces/IErrorMsg';
import { IMessage } from '../../interfaces/IMessage';
import { DomainClass } from './DomainClass';

export class MessagingService {

    private erroType: ErroType;
    private domain: DomainClass;
    private attr: Array<string> | string;
    private bodyMessage = new IErrorMsg();
    private imessage: IMessage;

    constructor(fields:{
        erroType: ErroType,
        domain: DomainClass,
        attr?: Array<string> | string
    }){
        Object.assign(this, fields);
    }
    set setErrotype(value: ErroType){
        this.erroType = value;
    }
    set setAttrErro(value: Array<string> | string){
        this.attr = value;
    }
    message(value: IMessage): IErrorMsg{
        this.imessage = value;
        this.bodyMessage.code = this.imessage.code;
        this.bodyMessage.status = 0;
        this.bodyMessage.level = this.erroType;
        this.bodyMessage.message = this.attr ? `parameters require ${this.attr}`: this.imessage.message;
        return this.bodyMessage;
    }
}