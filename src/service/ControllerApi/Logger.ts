import { Logs } from "../../models/Logs";
import { DomainClass } from "./DomainClass";

export class Logger {
    private LevelErr: number;
    private domain: DomainClass;
    private message: string;

    constructor(fields:{
        LevelErr: number,
        domain: DomainClass,
        message: string
    }){
        Object.assign(this, fields);
    }

    public register = async (): Promise<void> =>{
        let logs = new Logs();
        logs.LevelErr = this.LevelErr;
        logs.domain = this.domain;
        logs.message = this.message;
        logs.save();
    }
}