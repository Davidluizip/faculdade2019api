'use strict'
export enum Operation {
    login = 0,
    Parameter = 1,
    GenerateToken = 2,
    FirstAccess = 3,
    RefreshToken = 4,
    Email = 5,
    RESETPASSWORD = 6,

}