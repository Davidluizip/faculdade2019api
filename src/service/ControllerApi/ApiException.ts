"use strict"

import { Logger } from "./Logger";
import { DomainClass } from "./DomainClass";
import { ErroType } from "./ErroType";

export class ApiException {

    private error: any;
    private ErroValidateSeq: IErrorValidateSequelize;
    private ErroSeq: IErrorSequelize;

    constructor(field?: {
        error: any
    }) {
        Object.assign(this, field);
    }
    static instance(error: any) {
        return new ApiException(error);
    }
    inspectError = async () => {

        if (typeof this.error.errors !== 'undefined') {
            console.log("ERRO::::::: 1")
            console.log("ERRO::::::: 1", this.error)
            this.ErroValidateSeq = this.error.errors[0];
            return this.errValidateSequelize();
        } else if (this.error.name === 'SequelizeDatabaseError') {
            console.log("ERRO::::::: 2")
            console.log("ERRO::::::: 2", JSON.stringify(this.error));
            this.ErroSeq = this.error;
            return this.errSequelize();
        } else if (typeof this.error.level !== 'undefined') {
            console.log("ERRO::::::: 3")
            console.log("ERRO::::::: 3", JSON.stringify(this.error));
            this.error.status = 400;
            return this.error;
        } else {
            console.log("ERRO::::::: 4")
            console.log("ERRO::::::: 4", this.error)
            return await this.errDefault();
        }
    }
    private errValidateSequelize() {
        return {
            code: 99,
            message: this.ErroValidateSeq.message,
            type: this.ErroValidateSeq.type,
            path: this.ErroValidateSeq.path,
            value: this.ErroValidateSeq.value,
            origin: this.ErroValidateSeq.origin,
            status: 400
        }
    }
    private errSequelize = async () => {
        await this.registerLog();

        return {
            code: 100,
            message: 'Entrar em contato com admin informando o code!',
            type: this.ErroSeq.name.substring(9, 30),
            path: 'david.oliveira@mjv.com.br',
            value: this.ErroSeq.parent.sqlMessage,
            origin: 'Database',
            status: 400
        }
    }
    private errDefault = async () => {
        await this.registerLog();

        return {
            code: 101,
            message: 'Entrar em contato com admin informando o code!',
            path: 'david.oliveira@mjv.com.br',
            origin: 'Erro interno não mapeado',
            status: 400
        }
    }
    private registerLog = async () => {
        await new Logger({
            LevelErr: ErroType.infrastructure,
            domain: DomainClass.ApiException,
            message: `${this.error}`
        }).register();
    }
}