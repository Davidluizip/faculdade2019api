"use strict"
import { Operation } from "./Operation";
import { ErroParam } from "./ErroParam";
import { MessagingService } from "./MessagingService";
import { ErroType } from "./ErroType";
import { validateAttribute } from "./validateAttribute";
import { Message } from "./Message";
import { DomainClass } from "./DomainClass";

const exception: MessagingService = new MessagingService({
    erroType: ErroType.parameters,
    domain: DomainClass.ValidateParameter
});

export class validateParameter {

    private operation: Operation;
    private body: any;
    private params: Array<string> = [];

    constructor(field: {
        operation: Operation,
        body: any
    }) {
        Object.assign(this, field);
    }
    public async checkBody() {
        return await this.validateParams(this.parameter());
    }
    private async validateParams(_params:Array<string>) {
        const checkParam = new validateAttribute({body: this.body})
        let _: string;

        _params.forEach(_param => {
            switch (_param) {
                case ErroParam.email:
                    _ = checkParam._Email();
                    if (_) this.params.push(_);
                    break;
                case ErroParam.password:
                    _ = checkParam._Password();
                    if (_) this.params.push(_);
                    break;
                case ErroParam.CPF:
                    _ = checkParam._CPF();
                    if (_) this.params.push(_);
                    break;
            
                default:
                    break;
            }
        });
        if (this.params.length > 0) {
            exception.setAttrErro = this.params;
            throw exception.message(Message.parameter);
        }
        return false;

    }
    private parameter(): Array<string> {
        let _: Array<string> = [];
        if (this.operation === Operation.login 
            || this.operation === Operation.RefreshToken
            || this.operation === Operation.Email) {
            _.push(ErroParam.email);
            if(this.operation === Operation.login) _.push(ErroParam.password);
        }
        if (this.operation == Operation.RESETPASSWORD) {
            _.push(ErroParam.CPF)
        }


        return _;
    }

}