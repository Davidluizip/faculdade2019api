export enum DomainClass {
    AuthRouter = 0,
    AuthService = 1,
    NotificationRouter = 2,
    EmailService = 3,
    ValidateParameter = 8,
    VerifyTokenHeader = 9,
    ApiException = 10,
    UserRouter = 11,
    UserService = 12,

}