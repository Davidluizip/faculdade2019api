import { ErroParam } from "./ErroParam";

export class validateAttribute{
    private body: any;

    constructor(field: {
        body: any
    }) {
        Object.assign(this, field);
    }
    _Email(): string {
        if (!this.body.email && this.body.email == null) { return ErroParam.email }
        return null;
    }
    _Password(): string {
        if (!this.body.password && this.body.password == null) { return ErroParam.password }
        return null;
    }
    _CPF(): string {
        if (!this.body.CPF && this.body.CPF == null) { return ErroParam.CPF }
        return null;
    }
}