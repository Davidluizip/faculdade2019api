"use strict"
//import * as moment from "moment";
import * as jwt from "jwt-simple";
import { ConnectionData } from "../infrastructure/ConnectionData";
import { IToken } from "../interfaces/IToken";

const moment = require('moment');

export class TokenService {
    private static _token: IToken = new IToken();

    static gerarTokenLogin(email: string): IToken {

        let _token_expires = moment.tz('America/Sao_Paulo').utc().add({ minute: 5 }).unix();
        let _refresh_token_expires = moment.tz('America/Sao_Paulo').utc().add({ minute: 7 }).unix();

        this._token.token = `JWT ${jwt.encode({
            exp: _token_expires,
            email: email
        }, ConnectionData.secretToken)}`;

        this._token.refresh_token = `JWT ${jwt.encode({
            exp: _refresh_token_expires,
            email: email
        }, ConnectionData.secretToken)}`;

        this._token.token_expires = moment.unix(_token_expires).tz('America/Sao_Paulo').format();
        this._token.refresh_token_expires = moment.unix(_refresh_token_expires).tz('America/Sao_Paulo').format();

        return this._token;

    }
}