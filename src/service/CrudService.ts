export default class CrudService{
    private model: any;

    constructor(model: any) {
        this.model = model;
    }

    public async getAll(fields: {
        pagination?: IPagination,
        orderby?: IOrderBy
    }){
        return await this.model.findAndCountAll({
            order: [[fields.orderby.field, fields.orderby.type]],
            limit: fields.pagination.limit,
            offset: fields.pagination.offset
        });
    }

    public async getById(id: Number){
        return await this.model.findOne({ where: { id }});
    }

    public async update(id: Number, updates: any){
        const result = await this.model.update(updates, { where: { id } });

        return { rowsAffected: result[0] };
    }

    public async create(data: any){
        return await this.model.create(data);
    }

    public async delete(id: Number){
        const rowsAffected = await this.model.destroy({ where: { id } });

        return { rowsAffected };
    }

    

}